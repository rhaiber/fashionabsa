# +
config = {
    'domain': 'fashion',
    'device': 'cpu'
}

#language model for vocabulary generation
bert_mapper = {
    'fashion': './datasets/fashion/DK_BERT' #for training domain knowledge
    #'fashion': 'dbmdz/bert-base-german-cased' #for training with vanilla dbmdz BERT
}

#language model for NN-embeddings
bert_mapper_2 = {
    'fashion': './datasets/fashion/DK_BERT' #for training domain knowledge
    #'fashion': 'dbmdz/bert-base-german-uncased' #for training with vanilla dbmdz BERT
    
}
path_mapper = {
    'fashion': './datasets/fashion',
  
}
aspect_category_mapper = {
    'fashion': ['o', 'a', 'q', 'pa', 'm', 'pr']
}
aspect_seed_mapper = {
    'fashion': {
        'o': {"Look", "Farbe", "Muster", "Design", "Optik", "weiß", "schwarz", "rot","schön","aussehen"},
        'a': {"Lieferung", "Paket", "Service", "Händler", "schnell", "unkompliziert"},
        'q': {"Verarbeitung", "kaputt", "hochwertig", "Riss", "Qualität","eingegangen", "riecht", "waschen", "qualitativ"},
        'pa': {"Größe", "ausfallen" , "liegt", "Schnitt", "Sitz", "eng", "liegt", "passt", "geschnitten", "skinny", "kurz"},
        'm': {"dünn", "Seide", "Wolle", "Material", "Stoff", "Leder","weich"},
        'pr': {"Leistung", "Preis", "Wucher", "preiswert", "Kosten", "lohnt", "teuer"}
    }
}
sentiment_category_mapper = {
    'fashion': ['n', 'p']
}
sentiment_seed_mapper = {
     'fashion': {
        'n': {"schlecht", "schrecklich", "mies", "teuer", "hässlich", "zurück", "unangenehm", "unbequem", "kratzig", "leider", "altmodisch"},
        'p': {"gern","super", "mag", "toll", "schön", "gut", "liebe", "perfekt", "hübsch", "warm", "bequem", "günstig", "wertig", "problemlos", "top", "modisch"}
    }
}
M = {
    'fashion': 150
}

K_1 = 20 #Top K replacement candidates in MLM for seed words
K_2 = 20 #Top K replacement candidates in MLM for potential aspects/ potential sentiments



lambda_threshold = 1 
batch_size = 32 
validation_data_size = 100
learning_rate = 1e-5
epochs = 20






