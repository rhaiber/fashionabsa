import torch
from torch import nn
import torch.nn.functional as F
from transformers import BertModel
from config import *


class LQLoss(nn.Module):
    
    def __init__(self, q, weight, alpha=0.0):
        super().__init__()
        self.q = q ## parameter in the paper
        self.alpha = alpha ## hyper-parameter for trade-off between weighted and unweighted GCE Loss
        self.weight = nn.Parameter(F.softmax(torch.log(1 / torch.tensor(weight, dtype=torch.float32)), dim=-1), requires_grad=False).to('cpu') ## per-class weights , ## added dtype, ##changed "cuda" to device

    def forward(self, input, target, *args, **kwargs):
        bsz, _ = input.size()

        Yq = torch.gather(input, 1, target.unsqueeze(1))
        lq = (1 - torch.pow(Yq, self.q)) / self.q

        _weight = self.weight.repeat(bsz).view(bsz, -1)
        _weight = torch.gather(_weight, 1, target.unsqueeze(1))
    
        return torch.mean(self.alpha * lq + (1 - self.alpha) * lq * _weight)

class BERTLinear(nn.Module):

    def __init__(self, bert_type, num_cat, num_pol):
        super().__init__()
        self.bert = BertModel.from_pretrained(
            bert_type, output_hidden_states=True)
        print("BERT model used for embeddings: " )
        print(bert_type)
        
        ##need to be adjusted to be set in configs
        if config['domain']== 'fashion':
            self.ff_cat = nn.Linear(768, num_cat) 
            self.ff_pol = nn.Linear(768, num_pol) 
            self.aspect_weights = [630, 203, 224, 766, 850, 195] 
            self.sentiment_weights = [1019, 1849] 
            
    def forward(self, labels_cat, labels_pol, **kwargs):
        outputs = self.bert(**kwargs)
        x = outputs[2][11]  # (bsz, seq_len, 768)

        mask = kwargs['attention_mask']  # (bsz, seq_len)
        se = x * mask.unsqueeze(2)
        den = mask.sum(dim=1).unsqueeze(1)
        se = se.sum(dim=1) / den  # (bsz, 768)

        logits_cat = self.ff_cat(se)  # (bsz, num_cat)
        logits_pol = self.ff_pol(se)  # (bsz, num_pol)
        loss = torch.nn.CrossEntropyLoss()
        loss = LQLoss(0.4, self.aspect_weights)(F.softmax(logits_cat,dim=1), labels_cat) + LQLoss(0.4, self.sentiment_weights)(F.softmax(logits_pol,dim=1), labels_pol)
        return loss, logits_cat, logits_pol


