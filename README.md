# UnsupervisedABSA
We propose a BERT based three-step mixed semi-supervised model, which jointly detects aspect and sentiment in a given review sentence. The first step takes a small set of seed words for each aspect and each sentiment class to construct class vocabulary for each class using a context-aware BERT masked language model. The second step extracts aspect/opinion term(s) using POS tags and constructed vocabularies in step one. In the last step, extracted aspect and opinion words are used as label data to train a BERT based joint deep neural network for aspect and sentiment classification.

## CASC
In this work, we leverage power of post-trained, domain knowledge BERT (DK-BERT) and present a simple and highly efficient semi-supervised hybrid CASC approach for Context aware Aspect category and Sentiment Classification. Our model is built in a simple three-step process: 
1. We take a small set of seed words for each aspect and each sentiment class and then construct class vocabulary for each class that contains semantically coherent words with the seed words using BERT masked language model (MLM).
2. We take unlabeled training corpus and extract potential aspects and opinion terms using POS tags and class vocabularies constructed in the previous step.
3. In the last step, We make use of extracted aspect and opinion term as label data and jointly train BERT based neural model for aspect and sentiment classification.

<img src="Framework.png" width=1200>

### Requirements

To install all dependencies:
```bash
pip install -r requirements.txt
```

### Quick start
Run command:
```bash
python main.py
```

### Datasets
Prepared datasets for 'fashion' domain is available under `datasets/` directory

### Configuration
All configuration and model hyperparameters can be found at `config.py`.

**Configuring Domain**
```python
config = {
    'domain': 'fashion',
    'device': 'cpu'
}
```
The `domain` attribute determines which *domain* is used for training the model, which can be set to `fashion` or a new to define category. Moreover, `device` can be set to `cuda` for training model on GPU.

**Configuring data path**
```python
path_mapper = {
    'fashion': './datasets/fashion',
    'example': './datasets/example'
}
```
The `path_mapper` is responsible for providing root directory paths for each domain. The root directory should contain 3 files namely, `train_sent_cased.txt`(consisting of cased line separatd review sentences), `train_rev_cased.txt`(consisting of cased line separated reviews from the corpus) and `test.txt` (with `serial number`, `aspect category`, `sentiment category` and `review sentence` separated by tabs for each test example).

**Providing seed words**
`aspect_seed_mapper` and `sentiment_seed_mapper` are used for providing seed words for aspect and sentiment classes for each domain.


```
